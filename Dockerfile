FROM openjdk:10

COPY build/install/ambiance /home/ambiance/ambiance
COPY music /home/ambiance/music
COPY soundEffects /home/ambiance/soundEffects

RUN cd /home/ambiance/
CMD ["/home/ambiance/ambiance/bin/ambiance", "INSERT_TOKEN_HERE", "/home/ambiance/music", "/home/ambiance/soundEffects"]