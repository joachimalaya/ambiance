package ambiance.main

import java.io.File

val CONFIG_LOCATION = File("./config.json").toPath()

/**
 * An AmbianceConfig is a container for information specific to a [sx.blah.discord.handle.obj.IGuild] to which the bot was connected.
 */
class AmbianceConfig(
    var guildId: Long = 0,
    var shuffle: Boolean = false,
    var loop: Boolean = false,
    var toPlay: List<File> = listOf()
)