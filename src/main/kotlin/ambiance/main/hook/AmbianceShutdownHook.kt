package ambiance.main.hook

import ambiance.main.AmbianceConfig
import ambiance.main.CONFIG_LOCATION
import ambiance.main.JSON_MAPPER
import ambiance.main.LOG
import ambiance.main.MUSIC_MANAGERS
import sx.blah.discord.api.IDiscordClient

/**
 * Hook, that is called when the JVM is ordered to terminate.
 */
class AmbianceShutdownHook(private val client: IDiscordClient) : Thread() {

    override fun run() {
        LOG.debug("shutting down...")
        client.logout()
        val ambianceConfigs = MUSIC_MANAGERS
                .onEach { it.value.player.stopTrack() }
                .map { entry -> AmbianceConfig(entry.key.longID, entry.value.scheduler.shuffle, entry.value.scheduler.shuffle, entry.value.scheduler.tracks) }.toTypedArray()
        JSON_MAPPER.writeValue(CONFIG_LOCATION.toFile(), ambianceConfigs)
    }
}