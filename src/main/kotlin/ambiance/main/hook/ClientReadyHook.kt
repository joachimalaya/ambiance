package ambiance.main.hook

import ambiance.bot.DiscordEventListener
import ambiance.main.AmbianceConfig
import ambiance.main.CONFIG_LOCATION
import ambiance.main.JSON_MAPPER
import ambiance.main.LOG
import ambiance.main.MUSIC_MANAGERS
import ambiance.main.PLAYER_MANAGER
import ambiance.music.MusicManager
import sx.blah.discord.api.IDiscordClient

class ClientReadyHook(private val client: IDiscordClient) : Thread() {

    override fun run() {
        while (!client.isReady) {
            Thread.sleep(100)
        }
        LOG.debug("client ready; setting up listener and MusicManagers")
        client.guilds.forEach { MUSIC_MANAGERS[it] = MusicManager(PLAYER_MANAGER, client) }
        client.dispatcher.registerListener(DiscordEventListener())

        if (CONFIG_LOCATION.toFile().exists()) {
            JSON_MAPPER.readValue<Array<AmbianceConfig>>(CONFIG_LOCATION.toFile(), Array<AmbianceConfig>::class.java).forEach { ambianceConfig ->
                val musicManager = MUSIC_MANAGERS[MUSIC_MANAGERS.keys.find { it.longID == ambianceConfig.guildId }!!]!!
                musicManager.scheduler.shuffle = ambianceConfig.shuffle
                musicManager.scheduler.loop = ambianceConfig.loop
                musicManager.scheduler.tracks = ambianceConfig.toPlay.toMutableList()
            }
        }
    }
}