package ambiance.main

import ambiance.bot.function.Add
import ambiance.bot.function.Cast
import ambiance.bot.function.Clear
import ambiance.bot.function.Help
import ambiance.bot.function.Join
import ambiance.bot.function.Loop
import ambiance.bot.function.Ls
import ambiance.bot.function.LsSfx
import ambiance.bot.function.Next
import ambiance.bot.function.Play
import ambiance.bot.function.Sfx
import ambiance.bot.function.ShadowrunCast
import ambiance.bot.function.Shuffle
import ambiance.bot.function.State
import ambiance.bot.function.TogglePause
import ambiance.bot.function.Tree
import ambiance.bot.function.Volume
import ambiance.bot.function.resulthandler.SoundEffectResultHandler
import ambiance.main.hook.AmbianceShutdownHook
import ambiance.main.hook.ClientReadyHook
import ambiance.music.MusicManager
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import org.slf4j.log4j12.Log4jLoggerFactory
import sx.blah.discord.api.ClientBuilder
import sx.blah.discord.handle.obj.IGuild
import java.io.File
import java.nio.file.Files
import java.util.concurrent.ConcurrentHashMap
import java.util.function.BiPredicate

fun main(args: Array<String>) = mainBody {
    ArgParser(args).parseInto(::AmbianceArgs).run {
        // CLI management
        MUSIC_DIRECTORY = musicDirectory
        REJOIN_GREETER = rejoinGreeter
        SOUND_EFFECTS_DIRECTORY = soundEffectsDirectory
        DICE_THROW_EFFECT_NAME = diceThrowEffectName

        LOG.info("working on MUSIC_DIRECTORY: $MUSIC_DIRECTORY, SOUND_EFFECTS_DIRECTORY: $SOUND_EFFECTS_DIRECTORY")
        AudioSourceManagers.registerLocalSource(PLAYER_MANAGER)

        val client = ClientBuilder().withToken(token).build()
        client.login()

        Files.find(SOUND_EFFECTS_DIRECTORY.toPath(), 100, BiPredicate { _, attributes -> attributes.isRegularFile }).forEach {
            PLAYER_MANAGER.loadItem(it.toAbsolutePath().toString(), SoundEffectResultHandler())
        }

        ClientReadyHook(client).start()
        Runtime.getRuntime().addShutdownHook(AmbianceShutdownHook(client))
    }
}

const val ORDER_PREFIX = "/"

val LOG = Log4jLoggerFactory().getLogger("ambiance")!!
lateinit var MUSIC_DIRECTORY: File
lateinit var SOUND_EFFECTS_DIRECTORY: File
lateinit var DICE_THROW_EFFECT_NAME: String

var REJOIN_GREETER = false

val MESSAGE_FUNCTIONS = listOf(
        Help(),
        Join(),
        Play(),
        Add(),
        TogglePause(),
        Next(),
        Clear(),
        Cast(),
        ShadowrunCast(),
        State(),
        Loop(),
        Shuffle(),
        Volume(),
        Ls(),
        Tree(),
        LsSfx(),
        Sfx()
)
val MUSIC_MANAGERS = mutableMapOf<IGuild, MusicManager>()

val SOUND_EFFECTS = ConcurrentHashMap<String, AudioTrack>()
val JSON_MAPPER: ObjectMapper = ObjectMapper(JsonFactory()).enable(SerializationFeature.INDENT_OUTPUT)

val PLAYER_MANAGER = DefaultAudioPlayerManager()