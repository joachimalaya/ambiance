package ambiance.main

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.InvalidArgumentException
import com.xenomachina.argparser.default
import java.io.File

class AmbianceArgs(parser: ArgParser) {

    val token by parser.positional("TOKEN", "OAuth token")
    val musicDirectory by parser.positional("MUSIC_DIRECTORY", "base directory for music data") { File(this) }.default(File("./music")).addValidator {
        if (!value.exists() || !value.isDirectory) {
            throw InvalidArgumentException("MUSIC_DIRECTORY must be an existing directory but got ${value.absolutePath}")
        }
    }

    val soundEffectsDirectory by parser.positional("SOUND_EFFECTS_DIRECTORY", "directory for sound effects") { File(this) }.default(File("./soundEffects")).addValidator {
        if (!value.isDirectory) {
            throw InvalidArgumentException("SOUND_EFFECTS_DIRECTORY must be an existing directory but got ${value.absolutePath}")
        }
    }

    val diceThrowEffectName by parser.storing("-d", help = "file name of the dice throw sound effect").default("diceThrow.wav")

    val rejoinGreeter by parser.flagging("-g", help = "greet people when rejoining").default(false)
}