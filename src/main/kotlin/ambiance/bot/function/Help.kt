package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.main.MESSAGE_FUNCTIONS

class Help : BotFunction {

    override val order = "help"

    override fun accept(event: TokenizedMessageReceivedEvent) {
            val builder = StringBuilder()
            builder.append("```\n")
            MESSAGE_FUNCTIONS.forEach { f -> builder.append(f.description()).append("\n") }
            builder.append("\n```")
            answer(event, builder.toString())
    }

    override fun description(): String {
        return paddedOrder(order) + "- show this"
    }
}
