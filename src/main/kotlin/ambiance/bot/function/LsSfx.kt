package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.main.SOUND_EFFECTS

class LsSfx : BotFunction {

    override val order = "lssfx"

    override fun description(): String = paddedOrder(order) + "- list registered sound effects"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        answer(event, SOUND_EFFECTS.keys.joinToString("\n"))
    }
}