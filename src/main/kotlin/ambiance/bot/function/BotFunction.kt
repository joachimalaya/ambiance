package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import sx.blah.discord.api.internal.json.objects.EmbedObject
import sx.blah.discord.util.RequestBuffer
import java.util.function.Consumer

interface BotFunction : Consumer<TokenizedMessageReceivedEvent> {

    fun description(): String

    fun paddedOrder(order: String) = order.padEnd(DESCRIPTION_PAD, ' ')

    fun answer(event: TokenizedMessageReceivedEvent, message: String? = null, embed: EmbedObject? = null) {
        RequestBuffer.request { event.base.channel.sendMessage(message, embed) }
    }

    val order: String

    companion object {
        const val DESCRIPTION_PAD = 24
    }
}
