package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.main.MUSIC_DIRECTORY
import ambiance.tree.TreePrintOption
import ambiance.tree.TreeVisitor
import java.nio.file.Files
import java.nio.file.Path

class Tree : BotFunction {

    override val order = "tree"

    override fun description(): String {
        return paddedOrder("$order [path]") + "- list contents of directories in a tree-like format"
    }

    override fun accept(event: TokenizedMessageReceivedEvent) {
        if (event.tokens.size > 1) {
            val resolved = MUSIC_DIRECTORY.toPath().resolve(event.tokens[1])
            if (resolved.toRealPath().startsWith(MUSIC_DIRECTORY.toPath().toRealPath())) {
                answer(event, listContents(resolved))
            } else {
                answer(event, "${event.tokens[1]} not a valid path")
            }
        } else {
            answer(event, listContents(MUSIC_DIRECTORY.toPath()))
        }
    }

    private fun listContents(path: Path): String {
        val treeVisitor = TreeVisitor(TreePrintOption.DIRECTORIES)
        Files.walkFileTree(path, treeVisitor)

        val relativizedPath = if (path.toAbsolutePath() != MUSIC_DIRECTORY.toPath().toAbsolutePath()) MUSIC_DIRECTORY.toPath().relativize(path).toString() else "<root>"
        var message = "contents of $relativizedPath:\n" + treeVisitor.tree()
        if (message.length > 2000) {
            message = message.take(1997) + "..."
        }
        return message
    }
}