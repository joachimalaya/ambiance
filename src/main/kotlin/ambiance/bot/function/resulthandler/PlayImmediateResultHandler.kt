package ambiance.bot.function.resulthandler

import ambiance.main.LOG
import ambiance.music.PlaybackScheduler
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.handle.obj.ActivityType
import sx.blah.discord.handle.obj.StatusType

class PlayImmediateResultHandler(private val client: IDiscordClient, private val player: AudioPlayer) : AudioLoadResultHandler {

    override fun trackLoaded(track: AudioTrack) {
        LOG.info("found the track; will append")

        player.startTrack(track, false)
        client.changePresence(StatusType.ONLINE, ActivityType.PLAYING, PlaybackScheduler.readableName(track))
    }

    override fun playlistLoaded(playlist: AudioPlaylist) {
        // unused
    }

    override fun noMatches() {
        // base.getChannel().sendMessage("could not find resource " + resource);
    }

    override fun loadFailed(exception: FriendlyException) {
        LOG.info("could not open resource", exception)
    }
}
