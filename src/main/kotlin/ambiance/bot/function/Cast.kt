package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.main.DICE_THROW_EFFECT_NAME
import ambiance.main.SOUND_EFFECTS
import java.util.Random
import java.util.regex.Pattern

class Cast : BotFunction {

    override val order = "cast"

    private val rng: Random = Random()

    override fun accept(event: TokenizedMessageReceivedEvent) {
        val matcher = Pattern.compile(DICE_PATTERN).matcher(event.tokens[1].trim { it <= ' ' })
        if (matcher.matches()) {
            event.musicManager.scheduler.playEffect(SOUND_EFFECTS[DICE_THROW_EFFECT_NAME]!!)

            val amount = if (matcher.group(1).isEmpty()) 1 else Integer.parseInt(matcher.group(1))
            val sides = Integer.parseInt(matcher.group(2))
            val bonus = if (matcher.group(3) == null) 0 else Integer.parseInt(matcher.group(3))

            var sum = 0
            for (i in 0 until amount) {
                sum += rng.nextInt(sides) + 1
            }
            sum += bonus

            answer(event, "rolling ${event.tokens[1]} resulted in $sum")
        } else {
            event.base.channel
                    .sendMessage("${event.tokens[1]} is not a valid dice cast; must match Java Pattern $DICE_PATTERN")
        }
    }

    override fun description(): String {
        return paddedOrder("$order [m]d[n]+[x]") + "- throws m dice with n sides and adds x"
    }

    companion object {
        private const val DICE_PATTERN = "(\\d*)d(\\d+)([+|-]\\d+)?"
    }
}
