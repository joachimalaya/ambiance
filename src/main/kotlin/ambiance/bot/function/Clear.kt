package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent

class Clear : BotFunction {

    override val order = "clear"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.scheduler.tracks = mutableListOf()
    }

    override fun description(): String {
        return paddedOrder(order) + "- clear the current list of tracks"
    }
}
