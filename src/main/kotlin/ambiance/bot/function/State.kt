package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.music.PlaybackScheduler
import sx.blah.discord.util.EmbedBuilder
import java.awt.Color

class State : BotFunction {

    override val order = "state"

    override fun accept(event: TokenizedMessageReceivedEvent) {
            val builder = EmbedBuilder()
            builder.withColor(Color.CYAN)

        builder.appendField("looping", event.musicManager.scheduler.loop.toString(), true)
        builder.appendField("shuffling", event.musicManager.scheduler.shuffle.toString(), true)
        builder.appendField("paused", event.musicManager.player.isPaused.toString(), true)

        if (event.musicManager.scheduler.notPlayed.isEmpty()) {
                builder.appendField("left to play", "nothing", false)
            } else {
            builder.appendField("left to play", event.musicManager.scheduler.notPlayed.joinToString(", ") { f -> PlaybackScheduler.readableName(f) }, false)
            }

            answer(event, embed = builder.build())
    }

    override fun description(): String {
        return paddedOrder(order) + "- displays what the music player currently does"
    }
}
