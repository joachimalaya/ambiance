package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.main.LOG
import ambiance.music.AudioProvider
import sx.blah.discord.util.MissingPermissionsException

class Join : BotFunction {

    override val order = "join"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.base.guild.audioManager.audioProvider = AudioProvider(event.musicManager.player)
            val channels = event.base.guild.getVoiceChannelsByName(event.tokens[1])
            if (channels.isEmpty()) {
                answer(event, "unable to find voice channel named ${event.tokens[1]}")
            }
            val channel = channels[0]
            try {
                channel.join()
            } catch (mpe: MissingPermissionsException) {
                answer(event, "insufficient permissions to enter voice channel ${event.tokens[1]}")
                LOG.warn("failed to join requested channel", mpe)
            }
    }

    override fun description(): String {
        return paddedOrder("$order [voiceChannel]") + "- asks bot to join given voice channel"
    }
}
