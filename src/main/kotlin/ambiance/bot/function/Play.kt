package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent
import ambiance.main.JSON_MAPPER
import ambiance.main.LOG
import ambiance.main.MUSIC_DIRECTORY
import ambiance.music.playlist.AmbiancePlaylist
import java.io.IOException
import java.nio.file.Paths

class Play : BotFunction {

    override val order = "play"

    override fun accept(event: TokenizedMessageReceivedEvent) {
            val resource = Paths.get(MUSIC_DIRECTORY.toString(), event.tokens[1]).toFile()
            if (resource.exists()) {
                event.musicManager.scheduler.tracks.clear()

                if (resource.isDirectory) {
                    // iterate over directory contents
                    event.musicManager.scheduler.tracks = resource.listFiles().toMutableList()
                } else if (resource.name.matches(".+\\.json".toRegex())) {
                    // parse playlist
                    LOG.info("reading a playlist")
                    try {
                        val playlist = JSON_MAPPER.readValue(resource, AmbiancePlaylist::class.java)

                        // iterate over playlist members
                        event.musicManager.scheduler.tracks = playlist.tracksAsFiles(MUSIC_DIRECTORY.toPath()).toMutableList()
                        event.musicManager.scheduler.loop = playlist.isLoop
                        event.musicManager.scheduler.shuffle = playlist.isShuffle
                    } catch (ioe: IOException) {
                        LOG.warn("unable to access file $resource", ioe)
                        answer(event, "unable to access file $resource")
                    } catch (e: Exception) {
                        LOG.warn("unable to parse file $resource", e)
                    }
                } else {
                    // normal mechanism
                    event.musicManager.scheduler.tracks = mutableListOf(MUSIC_DIRECTORY.toPath().resolve(event.tokens[1]).toFile())
                }

                // start playback
                event.musicManager.scheduler.nextTrack()
        }
    }

    override fun description(): String {
        return paddedOrder("$order [path]") + "- replace files in playback list with the ones denoted by path; automatically starts playback"
    }
}
