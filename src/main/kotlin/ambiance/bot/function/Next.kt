package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent

class Next : BotFunction {

    override val order = "next"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.scheduler.nextTrack()
    }

    override fun description(): String {
        return paddedOrder(order) + "- start playing the next track in queue if any"
    }
}
