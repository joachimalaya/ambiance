package ambiance.bot.function

import ambiance.bot.TokenizedMessageReceivedEvent

class TogglePause : BotFunction {

    override val order = "pause"

    override fun accept(event: TokenizedMessageReceivedEvent) {
        event.musicManager.player.isPaused = !event.musicManager.player.isPaused
    }

    override fun description(): String {
        return paddedOrder(order) + "- pause current track"
    }
}
