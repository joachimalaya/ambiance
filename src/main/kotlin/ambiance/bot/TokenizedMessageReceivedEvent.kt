package ambiance.bot

import ambiance.main.MUSIC_MANAGERS
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent

class TokenizedMessageReceivedEvent(val base: MessageReceivedEvent) {

    val tokens = base.message.content.split(" ".toRegex()).filter { it.isNotEmpty() }.toTypedArray()
    val musicManager = MUSIC_MANAGERS[base.guild]!!
}