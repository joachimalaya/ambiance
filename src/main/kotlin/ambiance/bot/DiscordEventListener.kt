package ambiance.bot

import ambiance.main.LOG
import ambiance.main.MESSAGE_FUNCTIONS
import ambiance.main.ORDER_PREFIX
import ambiance.main.REJOIN_GREETER
import sx.blah.discord.api.events.EventSubscriber
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent
import sx.blah.discord.handle.obj.StatusType

class DiscordEventListener {

    @EventSubscriber
    fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.message.content.startsWith(ORDER_PREFIX)) {
            val tokenized = TokenizedMessageReceivedEvent(event)
            LOG.info("received tokens " + tokenized.base.message)
            MESSAGE_FUNCTIONS.filter {
                tokenized.tokens[0] == ORDER_PREFIX + it.order
            }.forEach { it.accept(tokenized) }
        } else {
            LOG.info("received message ${event.message} is not a valid order")
        }
    }

    // experimental notification on rejoin
    @EventSubscriber
    fun onPresenceUpdated(event: PresenceUpdateEvent) {
        if (REJOIN_GREETER && event.newPresence.status == StatusType.ONLINE && event.oldPresence.status != StatusType.ONLINE) {
            val guild = event.client.guilds[0]
            val generalChannel = guild.getChannelsByName("general")[0]
            generalChannel.sendMessage("welcome back " + event.user.getDisplayName(guild), true)
        }
    }
}