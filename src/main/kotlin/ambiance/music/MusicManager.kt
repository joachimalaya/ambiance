package ambiance.music

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import sx.blah.discord.api.IDiscordClient

/**
 * Central authority for everything dealing with music.
 */
class MusicManager(manager: AudioPlayerManager, client: IDiscordClient) {

    val player: AudioPlayer = manager.createPlayer()

    val scheduler: PlaybackScheduler = PlaybackScheduler(client, player, manager)

    init {
        player.addListener(scheduler)
    }
}